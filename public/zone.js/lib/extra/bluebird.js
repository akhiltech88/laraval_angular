(function (_global) {
    var __symbol__ = Zone['__symbol__'];
    Zone[__symbol__('bluebird')] = function patchBluebird(Bluebird) {
        Bluebird.setScheduler(function (fn) {
            Zone.current.scheduleMicroTask('bluebird', fn);
        });
    };
})(typeof window === 'object' && window || typeof self === 'object' && self || global);
//# sourceMappingURL=bluebird.js.map