"use strict";
var utils_1 = require('../common/utils');
var fs;
try {
    fs = require('fs');
}
catch (err) {
}
var TO_PATCH_MACROTASK_METHODS = [
    'access', 'appendFile', 'chmod', 'chown', 'close', 'exists', 'fchmod',
    'fchown', 'fdatasync', 'fstat', 'fsync', 'ftruncate', 'futimes', 'lchmod',
    'lchown', 'link', 'lstat', 'mkdir', 'mkdtemp', 'open', 'read',
    'readdir', 'readFile', 'readlink', 'realpath', 'rename', 'rmdir', 'stat',
    'symlink', 'truncate', 'unlink', 'utimes', 'write', 'writeFile',
];
if (fs) {
    TO_PATCH_MACROTASK_METHODS.filter(function (name) { return !!fs[name] && typeof fs[name] === 'function'; })
        .forEach(function (name) {
        utils_1.patchMacroTask(fs, name, function (self, args) {
            return {
                name: 'fs.' + name,
                args: args,
                callbackIndex: args.length > 0 ? args.length - 1 : -1,
                target: self
            };
        });
    });
}
//# sourceMappingURL=fs.js.map