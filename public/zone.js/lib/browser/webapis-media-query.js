(function (_global) {
    patchMediaQuery(_global);
    function patchMediaQuery(_global) {
        if (!_global['MediaQueryList']) {
            return;
        }
        var patchEventTargetMethods = Zone[Zone['__symbol__']('patchEventTargetMethods')];
        patchEventTargetMethods(_global['MediaQueryList'].prototype, 'addListener', 'removeListener', function (self, args) {
            return {
                useCapturing: false,
                eventName: 'mediaQuery',
                handler: args[0],
                target: self || _global,
                name: 'mediaQuery',
                invokeAddFunc: function (addFnSymbol, delegate) {
                    if (delegate && delegate.invoke) {
                        return this.target[addFnSymbol](delegate.invoke);
                    }
                    else {
                        return this.target[addFnSymbol](delegate);
                    }
                },
                invokeRemoveFunc: function (removeFnSymbol, delegate) {
                    if (delegate && delegate.invoke) {
                        return this.target[removeFnSymbol](delegate.invoke);
                    }
                    else {
                        return this.target[removeFnSymbol](delegate);
                    }
                }
            };
        });
    }
})(typeof window === 'object' && window || typeof self === 'object' && self || global);
//# sourceMappingURL=webapis-media-query.js.map