(function (_global) {
    patchNotification(_global);
    function patchNotification(_global) {
        var Notification = _global['Notification'];
        if (!Notification || !Notification.prototype) {
            return;
        }
        var desc = Object.getOwnPropertyDescriptor(Notification.prototype, 'onerror');
        if (!desc || !desc.configurable) {
            return;
        }
        var patchOnProperties = Zone[Zone['__symbol__']('patchOnProperties')];
        patchOnProperties(Notification.prototype, null);
    }
})(typeof window === 'object' && window || typeof self === 'object' && self || global);
//# sourceMappingURL=webapis-notification.js.map